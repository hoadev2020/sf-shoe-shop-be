import swaggerAutogen from "swagger-autogen";

const doc = {
  info: {
    title: "SF Shoe Shop API",
    description: "SF Shoe Shop API",
    version: "1.0.0",
  },
  host: "sf-shoe-shop-be.herokuapp.com",
  schemes: ["https", "http"],
  securityDefinitions: {
    default: {
      type: "apiKey",
      in: "header",
      name: "Authorization",
      description: "Authorization token user logged in",
    },
  },
};

const outputFile = "./backend/swagger.json";
const endpointsFiles = ["./backend/server.js"];

swaggerAutogen()(outputFile, endpointsFiles, doc);
