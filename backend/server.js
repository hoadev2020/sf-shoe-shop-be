import express from "express";
import dotenv from "dotenv";
import config from "./config";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import userRoute from "./routes/userRoute";
import productRoute from "./routes/productRoute";
import swaggerUi from "swagger-ui-express";
import swaggerDocument from "./swagger.json";
import orderRoute from "./routes/orderRoute";

dotenv.config();

const mongodbUrl = config.MONGODB_URL;
mongoose
  .connect(mongodbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .catch((error) => console.log(error.reason));

const app = express();
app.use(bodyParser.json());
app.use((_req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,Content-Type,Authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);

  next();
});

app.get("/", (_req, res) => {
  // #swagger.ignore = true
  res.send({
    "API Name": "Shoe Shop API",
    version: "v1.0.0",
    documents: "/api-docs",
  });
});
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(
  "/api/users",
  userRoute

  // #swagger.tags = ['Users']
);
app.use(
  "/api/products",
  productRoute

  // #swagger.tags = ['Products']
);
app.use(
  "/api/orders",
  orderRoute
  // #swagger.tags = ['Orders']
);

app.listen(process.env.PORT, () => {
  console.log(`Server started at port ${process.env.PORT}`);
});
