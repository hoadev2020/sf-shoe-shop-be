import express from "express";
import User from "../models/userModel";
import { getToken, isAdmin, isAuth } from "../util";

const router = express.Router();

router.post("/signin", async (req, res) => {
  const signinUser = await User.findOne({
    email: req.body.email,
    password: req.body.password,
  });

  if (signinUser) {
    res.send({
      _id: signinUser.id,
      name: signinUser.name,
      email: signinUser.email,
      isAdmin: signinUser.isAdmin,
      token: getToken(signinUser),
    });
  } else {
    res.status(401).send({ msg: "Invalid Email or Password" });
  }
});

router.post("/register", async (req, res) => {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });

  const userExists = await User.findOne({ email: req.body.email });

  if (userExists) {
    res.status(400).send("User already exists");
  }

  const newUser = await user.save();

  if (newUser) {
    res.send({
      _id: newUser.id,
      name: newUser.name,
      email: newUser.email,
      isAdmin: newUser.isAdmin,
      token: getToken(newUser),
    });
  } else {
    res.status(401).send({ msg: "Invalid User Data" });
  }
});

router.get("/createadmin", async (_req, res) => {
  try {
    const user = new User({
      name: "Hoang Hoa",
      email: "hoanghoa@gmail.com",
      password: "123456",
      isAdmin: true,
    });

    const newUser = await user.save();
    res.send(newUser);
  } catch (error) {
    res.send({ msg: error.message });
  }
});

router.get("/list", isAuth, isAdmin, async (_req, res) => {
  const users = await User.find().select("-password");
  res.send(users);
});

router.get("/:id", async (req, res) => {
  const user = await User.findById(req.params.id);
  if (user) {
    res.send({
      _id: user.id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    });
  } else {
    res.status(404).send({ message: "User Not Found" });
  }
});

router.put("/profile", isAuth, async (req, res) => {
  const user = await User.findById(req.user._id);
  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    if (req.body.password) {
      user.password = req.body.password;
    }
    const updatedUser = await user.save();
    res.send({
      _id: updatedUser.id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: getToken(updatedUser),
    });
  } else {
    res.status(404).send({ message: "User Not Found" });
  }
});

router.put("/update-profile/:id", isAuth, isAdmin, async (req, res) => {
  /* #swagger.parameters["user"] = {
    in: "body",
    description: "User info",
    required: true,
    type: "object",
    schema: {
      $name: "name",
      $email: "name@gmail.com",
      $isAdmin: "false",
      password: "password"
    }
  }; */
  const user = await User.findById(req.params.id);
  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    user.isAdmin = req.body.isAdmin || user.isAdmin;
    if (req.body.password) {
      user.password = req.body.password;
    }
    const updatedUser = await user.save();
    res.send({
      _id: updatedUser.id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: getToken(updatedUser),
    });
  } else {
    res.status(404).send({ message: "User Not Found" });
  }
});

router.delete("/:id", isAuth, isAdmin, async (req, res) => {
  const deleteUser = await User.findById(req.params.id);
  if (deleteUser) {
    await deleteUser.remove();
    res.send({ message: "User Deleted" });
  } else {
    res.status(404).send({ message: "Error in delete user" });
  }
});

export default router;
