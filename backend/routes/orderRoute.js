import express from "express";
import Order from "../models/orderModel";
import { isAdmin, isAuth } from "../util";

const router = express.Router();

router.get("/", async (_req, res) => {
  const orders = await Order.find();
  res.send(orders);
});

router.post("/", async (req, res) => {
  /* #swagger.parameters["addToCart"] = {
    in: "body",
    description: "Add to cart",
    schema: {
      $fullname: "Customer name",
      $phone_number: "09xx...",
      $email: "test@example.com",
      $province: "Province",
      $district: "District",
      $ward: "Ward",
      $address: "1st ...",
      $payment_method: "COD",
      $cartItems: [
        {
          name: "Product name",
          quantity: "1",
          image: "Product image link",
          price: "50000",
          size: "28",
          product: "UUID of product",
        },
      ],
    },
  }; */

  const cartItems = req.body.cartItems;
  if (cartItems && cartItems.length === 0) {
    return res.status(400).send("No order items");
  }
  try {
    const order = new Order({
      fullname: req.body.fullname,
      phone_number: req.body.phone_number,
      email: req.body.email,
      province: req.body.province,
      district: req.body.district,
      ward: req.body.ward,
      address: req.body.address,
      payment_method: req.body.payment_method,
      cartItems: req.body.cartItems,
    });
    const newOrder = await order.save();
    if (newOrder) {
      return res
        .status(201)
        .send({ message: "New order added", data: newOrder });
    }
  } catch (error) {
    res.status(500).send({
      message: "Error in creating order",
      error: error.message,
    });
  }
});
router.put("/:id", isAuth, isAdmin, async (req, res) => {
  /* #swagger.parameters["id"] = {
    in: "path",
    description: "Order ID.",
    required: true,
    type: "string",
  };*/
  /* #swagger.parameters["updateCart"] = {
    in: "body",
    description: "Update cart",
    schema: {
      $fullname: "Customer name",
      $phone_number: "09xx...",
      $email: "test@example.com",
      $province: "Province",
      $district: "District",
      $ward: "Ward",
      $address: "1st ...",
      $payment_method: "COD",
      $cartItems: [
        {
          name: "Product name",
          quantity: "1",
          image: "Product image link",
          price: "50000",
          size: "28",
          product: "UUID of product",
        },
      ],
    },
  }; */
  const orderId = req.params.id;
  const order = await Order.findById(orderId);

  if (order) {
    order.fullname = req.body.fullname;
    order.phone_number = req.body.phone_number;
    order.email = req.body.email;
    order.province = req.body.province;
    order.district = req.body.district;
    order.ward = req.body.ward;
    order.address = req.body.address;
    order.payment_method = req.body.payment_method;
    if (req.body.cartItems && req.body.cartItems.length > 0) {
      order.cartItems = req.body.cartItems;
    }

    try {
      const updatedOrder = await order.save();
      if (updatedOrder) {
        return res
          .status(201)
          .send({ message: "Order updated", data: newOrder });
      }
    } catch (error) {
      res.status(500).send({
        message: "Error in updating order",
        error: error.message,
      });
    }
  }
});

router.delete("/:id", isAuth, isAdmin, async (req, res) => {
  const deleteOrders = await Order.findById(req.params.id);
  if (deleteOrders) {
    await deleteOrders.remove();
    res.send({ message: "Order Deleted" });
  } else {
    res.status(404).send({ message: "Error in delete order" });
  }
});

export default router;
