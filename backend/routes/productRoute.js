import express from "express";
import Product from "../models/productModel ";
import { isAdmin, isAuth } from "../util";

const router = express.Router();

router.get("/", async (_req, res) => {
  const products = await Product.find();
  res.send(products);
});

router.get("/:id", async (req, res) => {
  const productId = req.params.id;
  try {
    const product = await Product.findById(productId);
    res.send(product);
  } catch (error) {
    res.status(404).send({ errors: error.message });
  }
});

router.post("/", isAuth, isAdmin, async (req, res) => {
  /* #swagger.parameters["createProduct"] = {
    in: "body",
    description: "Create new product",
    schema: {
      $name: "Product name",
      $price: "500000",
      $image: "Product image link",
      $brand: "Brand name",
      $category: "Category name",
      $countInStock: "1",
      $description: "Description",
      $rating: "5",
      $numReviews: "2",
      $sizeList: ["20", "29", "30"],
    },
  }; */
  try {
    const product = new Product({
      name: req.body.name,
      price: req.body.price,
      image: req.body.image,
      brand: req.body.brand,
      category: req.body.category,
      countInStock: req.body.countInStock,
      description: req.body.description,
      rating: req.body.rating,
      numReviews: req.body.numReviews,
      sizeList: req.body.sizeList,
    });
    const newProduct = await product.save();
    if (newProduct) {
      return res
        .status(201)
        .send({ message: "New product created", data: newProduct });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: "Error in creating product", errors: error.message });
  }
});

router.put("/:id", isAuth, isAdmin, async (req, res) => {
  /* #swagger.parameters["id"] = {
    in: "path",
    description: "Product ID.",
    required: true,
    type: "string",
  };*/
  /* #swagger.parameters["updateProduct"] = {
    in: "body",
    description: "Update product",
    schema: {
      $name: "Product name",
      $price: "500000",
      $image: "Product image link",
      $brand: "Brand name",
      $category: "Category name",
      $countInStock: "1",
      $description: "Description",
      $rating: "5",
      $numReviews: "2",
      $sizeList: ["20", "29", "30"],
    },
  }; */
  const productId = req.params.id;
  const product = await Product.findById(productId);
  if (product) {
    product.name = req.body.name;
    product.price = req.body.price;
    product.image = req.body.image;
    product.brand = req.body.brand;
    product.category = req.body.category;
    product.countInStock = req.body.countInStock;
    product.description = req.body.description;
    if (req.body.sizeList) {
      product.sizeList = req.body.sizeList;
    }
    try {
      const updatedProduct = await product.save();
      if (updatedProduct) {
        return res
          .status(200)
          .send({ message: "Product Updated", data: updatedProduct });
      }
    } catch (error) {
      return res.status(500).send({ message: "Error in updating Product" });
    }
  }
});

router.delete("/:id", isAuth, isAdmin, async (req, res) => {
  const deleteProducts = await Product.findById(req.params.id);
  if (deleteProducts) {
    await deleteProducts.remove();
    res.send({ message: "Product Deleted" });
  } else {
    res.status(404).send({ message: "Error in delete product" });
  }
});

export default router;
