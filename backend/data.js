export default {
    products: [
    {
        _id:'123',
        name: 'Slim Shirt',
        category: 'Shirts',
        image: '/images/d1.jpg',
        price: 60,
        brand: 'Nike',
        rating: 4.5,
        numReviews: 10,
        countInStock: 6
    },
    {
        _id:'124',
        name: 'Fit Shirt',
        category: 'Shirts',
        image: '/images/d1.jpg',
        price: 60,
        brand: 'Nike',
        rating: 4.5,
        numReviews: 10,
        countInStock: 1
    },{
        _id:'125',
        name: 'Best Pants',
        category: 'Pants',
        image: '/images/d1.jpg',
        price: 70,
        brand: 'Nike',
        rating: 4.5,
        numReviews: 10,
        countInStock: 2
    },
    {
        _id:'126',
        name: 'Blue Shirt',
        category: 'Shirt',
        image: '/images/d1.jpg',
        price: 100,
        brand: 'Adidas',
        rating: 4.5,
        numReviews: 10,
        countInStock: 0
    }
]
}