import mongoose from "mongoose";

const orderSchema = new mongoose.Schema({
  fullname: { type: String, required: true },
  phone_number: { type: String, required: true },
  email: { type: String, required: true },
  province: { type: String, required: true },
  district: { type: String, required: true },
  ward: { type: String, required: true },
  address: { type: String, required: true },
  payment_method: { type: String, required: true },
  cartItems: [
    {
      name: { type: String, required: true },
      quantity: { type: Number, required: true },
      image: { type: String, required: true },
      price: { type: Number, required: true },
      size: { type: Number, required: true },
      product: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Product",
      },
    },
  ],
});

const orderModel = mongoose.model("Order", orderSchema);
export default orderModel;
